# filesystem-test

[![Hackage](https://img.shields.io/hackage/v/filesystem-test.svg?logo=haskell)](https://hackage.haskell.org/package/filesystem-test)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)

Test filesystem IO in pure unit tests
