module System.Filesystem.Class (MonadFilesystem(..)) where


import qualified Data.Text.IO.Utf8 as Utf8
import qualified System.Directory


class MonadFilesystem m where
    read :: FilePath -> m (Maybe Text)
    write :: FilePath -> Text -> m ()



instance MonadFilesystem IO where
    read path = do
        exists <- System.Directory.doesFileExist path
        if exists
            then Just <$> Utf8.readFile path
            else return Nothing
    write = Utf8.writeFile
