module Test.Filesystem.FileTreeTest (spec_dataStructure,spec_monadFilesystemInstance) where

import Test.Filesystem.FileTree (FileTree)
import qualified Test.Filesystem.FileTree as FileTree
import Test.Tasty.Hspec
import System.FilePath (pathSeparator)
import qualified System.Filesystem.Class as Filesystem
import Test.Hspec.Core.Spec (Example(..))

(|>) :: t1 -> (t1 -> t2) -> t2
(|>) a f = f a


spec_dataStructure :: Spec
spec_dataStructure =
    describe "Data.FileTree" $ do
        it "can read and write a file" $ do
            (mempty :: FileTree String)
                |> FileTree.write "/file.txt" "{}"
                |> FileTree.read "/file.txt"
                `shouldBe` Just "{}"

        it "can read a non-existent file" $ do
            (mempty :: FileTree String)
                |> FileTree.read "/file.txt"
                `shouldBe` Nothing

        it "normalizes paths" $ do
            (mempty :: FileTree String)
                |> FileTree.write "/file.txt" "{}"
                |> FileTree.read (pathSeparator : "./file.txt")
                `shouldBe` Just "{}"

        describe "doesFileExist" $ do
            it "is false for non-existant" $ do
                (mempty :: FileTree String)
                    |> FileTree.doesFileExist "/file.txt"
                    `shouldBe` False

            it "is true for file" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/file.txt" ""
                    |> FileTree.doesFileExist "/file.txt"
                    `shouldBe` True

            it "is false for directory" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/path/file.txt" ""
                    |> FileTree.doesFileExist "/path"
                    `shouldBe` False

        describe "doesDirectoryExist" $ do
            it "is false for non-existant" $ do
                (mempty :: FileTree String)
                    |> FileTree.doesDirectoryExist "/file.txt"
                    `shouldBe` False

            it "is false for file" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/file.txt" ""
                    |> FileTree.doesDirectoryExist "/file.txt"
                    `shouldBe` False

            it "is true for directory" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/path/file.txt" ""
                    |> FileTree.doesDirectoryExist "/path"
                    `shouldBe` True

        describe "listDirectory" $ do
            it "includes files" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/path/file.txt" ""
                    |> FileTree.listDirectory "/path"
                    `shouldBe` ["file.txt"]

            it "includes directories" $ do
                (mempty :: FileTree String)
                    |> FileTree.write "/path/sub/file.txt" ""
                    |> FileTree.listDirectory "/path"
                    `shouldBe` ["sub"]


spec_monadFilesystemInstance :: Spec
spec_monadFilesystemInstance =
    describe "instance MonadFilesystem (StateT FileTree)" $ do
        it "can read and write a file" $ do
            return () :: State (FileTree Text) ()
            Filesystem.write "/file.txt" "{}"
            result <- Filesystem.read "/file.txt"
            return $ result `shouldBe` Just "{}"

        it "can read a non-existent file" $ do
            return () :: State (FileTree Text) ()
            result <- Filesystem.read "/file.txt"
            return $ result `shouldBe` Nothing

        it "normalizes paths" $ do
            return () :: State (FileTree Text) ()
            Filesystem.write "/file.txt" "{}"
            result <- Filesystem.read (pathSeparator : "./file.txt")
            return $ result `shouldBe` Just "{}"


instance Example e => Example (StateT (FileTree Text) Identity e) where
    type Arg (StateT (FileTree Text) Identity e) = Arg e
    evaluateExample =
        evaluateExample . flip evalState mempty
